$(document).ready(function () {
    $('#carouselHome').carousel({
        pause: 'none',
    });
    menuScroll();
    imgBgInlineStyle();
    menuHamburguer();
    mailValidation();
});

/**
 * Menu animación cuando pasa a fixed
 */
function menuScroll() {
    $(window).scroll(() => {
        let windowTop = $(window).scrollTop(),
            windowWidth = $(window).width(),
            mainMenu = $('#mainMenu'),
            mMenuContainer = $('.mainMenuContainer');

        if (windowWidth >= 1024) {
            if (windowTop > 1) {
                mainMenu.addClass('navbar--is-active');
                mainMenu.removeClass('navbar-dark');
                mMenuContainer.addClass('bg-light navbar-light');
                mainMenu.find('.navbar-bg').addClass('bbr-0');
            } else {
                mainMenu.removeClass('navbar--is-active bg-light');
                mainMenu.addClass('navbar-dark');
                mMenuContainer.removeClass('bg-light navbar-light');
                mainMenu.find('.navbar-bg').removeClass('bbr-0');
            }
        }
    });
}

function menuHamburguer() {
    $('.menu-link').click(function (e) {
        e.preventDefault();
        $('.menu-overlay').toggleClass('open');
        $('.menu').toggleClass('open');
        $('.mainMenuContainer').toggleClass('open overflow-hidden');
        $('.carousel-indicators').toggleClass('zIndex0');
        $('.carousel-control-prev').toggleClass('zIndex0');
        $('.carousel-control-next').toggleClass('zIndex0');
    });
}

/**
 * Obtener source de imagen y apliacar en inline style
 */
function imgBgInlineStyle() {
    $('.carousel-item').each(function (ix, vl) {
        let img = $(vl).find('img'),
            src = img.attr('src');
        $(vl).css('background-image', 'url(' + src + ')');
        img.remove();
    });
}

function mailValidation() {
    const form = $('#form-contact');
    let dataForm = [];
    form.submit(function (e) {
        e.preventDefault();
        console.log('hola');
        form.find('input').each(function (id, val) {
            dataForm.push($(val).val());
        });
        dataForm.push(document.forms['form-contact']['comments'].value);
        console.log(dataForm);

        $.ajax({
          method: "POST",
          url: "sendEmail.php",
          data: {dataForm},
          dataType: "text"
        }).done(function(res) {
          const cRes = parseInt(res);
          if (cRes) {
            swal({
              title: "¡Enviado!",
              text: "Pronto te responderemos, gracias por contactarnos",
              icon: "success",
              button: "Vale",
            });
          }else{
            swal({
              title: "¡Oh no!",
              text: "Algo ha salido mal, intenta más tarde",
              icon: "error",
              button: "De acuerdo",
            });
          }      
        });
    });

    
}
